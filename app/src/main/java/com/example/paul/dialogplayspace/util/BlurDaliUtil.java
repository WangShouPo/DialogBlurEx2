package com.example.paul.dialogplayspace.util;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.paul.dialogplayspace.R;

import at.favre.lib.dali.Dali;
import at.favre.lib.dali.builder.blur.BlurBuilder;

public class BlurDaliUtil {

    private Context context;
    private Dali dali;
    private String TAG = getClass().getSimpleName();

    public BlurDaliUtil(Context context) {
        this.context = context;
        dali = Dali.create(context);
    }

    public void simpleBlur1(int resourceId, ImageView targetImageView, TextView resultDescriptionTv) {
        // target view
//        final ImageView iv = binding.image;
        // blur resource to imageView
        BlurBuilder.JobDescription jobDescription1 =
                dali.load(resourceId)
                        .placeholder(R.drawable.test_img1)
                        .blurRadius(12)
                        .colorFilter(Color.parseColor("#ff00529c"))
                        .concurrent().into(targetImageView);
        // blur job Description
        if (resultDescriptionTv != null) {
            resultDescriptionTv.setText(jobDescription1.builderDescription);
        }
    }

    public void simpleBlur2(int resourceId, ImageView targetImageView, TextView resultDescriptionTv) {
        // target view
//        final ImageView iv = binding.image;
        // blur resource to imageView
        BlurBuilder.JobDescription jobDescription1 =
                dali.load(resourceId)
                        .placeholder(R.drawable.test_img1)
                        .blurRadius(12)
                        .brightness(0)
                        .concurrent().into(targetImageView);
        // blur job Description
        if (resultDescriptionTv != null) {
            resultDescriptionTv.setText(jobDescription1.builderDescription);
        }
    }

    // TODO: how to dark background, brightness(-40) not working
    public void simpleBlur5(TextView resultDescriptionTv) {
        final ViewGroup viewGroup = (ViewGroup) ((ViewGroup) ((Activity)context).findViewById(android.R.id.content)).getChildAt(0);
//        Log.d(TAG, "viewGroup:"+viewGroup.toString());
//        BlurBuilder.JobDescription jobDescription5 =
//                dali.load(viewGroup).blurRadius(8).downScale(4).brightness(-40).concurrent().reScale().into(mBlurIv);
        ImageView imageView = new ImageView(context);
        final ViewGroup rootView = ((ViewGroup) ((Activity)context).findViewById(android.R.id.content));
        rootView.addView(imageView);

        BlurBuilder.JobDescription jobDescription5 =
                dali.load(viewGroup).blurRadius(20).downScale(2).brightness(0).concurrent().reScale().skipCache().into(imageView);
        resultDescriptionTv.setText(jobDescription5.builderDescription);
    }
}
